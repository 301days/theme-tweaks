# theme-tweaks

A home for some WIP tweaks to website themes.

- jekyll: based on Jekyll 4.2.2 and the basically-basic theme.
- hugo/Hugo-Octopress: based on Hugo 0.111.3 and the Hugo-Octopress theme.
- hugo/Blowfish: based on Hugo 0.121.1+extended and the Blowfish theme v2.58.0.

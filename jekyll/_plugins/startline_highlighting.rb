# Monkey-patch to allow starting line in code highlights
# Inspiration: https://github.com/jekyll/jekyll/issues/8621#issuecomment-839054583
module Jekyll
    module Tags
      class HighlightBlock
        def render_rouge(code)
          require "rouge"
          formatter = Rouge::Formatters::HTMLLineTable.new(
            ::Rouge::Formatters::HTML.new,
            start_line: parse_startline(@highlight_options[:start_line])
          )
          lexer = ::Rouge::Lexer.find_fancy(@lang, code) || Rouge::Lexers::PlainText
          formatter.format(lexer.lex(code))
        end
  
        private
  
        def parse_startline(startline_string)
          return 1 if startline_string.nil?
  
          startline_string.to_i
        end
      end
    end
  end
# Fork of "Simple Code Blocks for Jekyll" https://github.com/imathis/octopress/blob/master/plugins/code_block.rb
require './_plugins/rouge_code'
require './_plugins/raw'

module Jekyll

  class CodeBlock < Liquid::Block
    CaptionUrlTitle = /(\S[\S\s]*)\s+(https?:\/\/\S+|\/\S+)\s*(.+)?/i
    CaptionUrlTitleNew = /(\S[\S\s]*)\s+<a[\S\s]*>(https?:\/\/\S+|\/\S+)<\/a>\s*(.+)?/i
    Caption = /(\S[\S\s]*)/
    def initialize(tag_name, markup, tokens)
      @title = nil
      @caption = nil
      @filetype = nil
      @starting_line = 1
      @highlight = true

      if markup =~ /\s*lang:(\S+)/i
        # puts "We found language #{$1}"
        @filetype = $1
        markup = markup.sub(/\s*lang:(\S+)/i,'')
      end
      # puts "Checking #{markup} against #{CaptionUrlTitle}..."
      before = Time.now
      if markup.index('<a href').nil? && markup =~ CaptionUrlTitle
      # if markup =~ CaptionUrlTitle
        # puts "We found CaptionUrlTitle file #{$1} code_url #{$2} linktext #{$3}"
        @file = $1
        code_url = $2
        @caption = "<figcaption><span>#{$1}</span><a href='#{$2}'>#{$3 || 'link'}</a></figcaption>"
        if code_url =~ /\S+#L(\d+)/
          @starting_line = $1.to_i
        end
      elsif markup =~ Caption
        # puts "We found Caption file #{$1}"
        @file = $1
        @caption = "<figcaption><span>#{$1}</span></figcaption>\n"
      end
      # puts "...that took #{Time.now - before} seconds."
      if @file =~ /\S[\S\s]*\w+\.(\w+)/ && @filetype.nil?
        # puts "We found filetype #{$1}"
        @filetype = $1
      end
      # puts "...done"
      super
    end

    def render(context)
      output = super
      code = super
      code_lines = code.lines
      # code_lines.each_with_index do |line,index|
      #   puts "raw code line #{index}, with #{line.length} chars, is \"#{line}\""
      #   if line.length < 20
      #     puts "   Dumped, it's #{line.dump}"
      #   end
      # end
      if code_lines[0] == "\n" || code_lines[0] == "\r\n"
        code_lines.shift
      end
      if code_lines.last == "\n" || code_lines.last == "\r\n"
        code_lines.pop
      end
      # code_lines.each_with_index do |line,index|
      #   puts "less raw code line #{index}, with #{line.length} chars, is \"#{line}\""
      #   if line.length < 20
      #     puts "   Dumped, it's #{line.dump}"
      #   end
      # end
      code = code_lines.join
      source = "<figure class='code'>"
      source += @caption if @caption
      if @filetype
        source += "#{HighlightCode::highlight(code, @filetype, @starting_line)}</figure>"
      else
        source += "#{HighlightCode::tableize_code(code.lstrip.rstrip.gsub(/</,'&lt;'))}</figure>"
      end
      source = TemplateWrapper::safe_wrap(source)
      source = context['pygments_prefix'] + source if context['pygments_prefix']
      source = source + context['pygments_suffix'] if context['pygments_suffix']
      source
    end
  end
end

Liquid::Template.register_tag('codeblock', Jekyll::CodeBlock)

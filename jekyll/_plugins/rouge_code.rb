# Conversion of https://github.com/imathis/octopress/blob/master/plugins/pygments_code.rb for use
#   with Rouge 3.x.
require 'fileutils'
require 'digest/md5'

# ROUGE_CACHE_DIR = File.expand_path('../../.rouge-cache', __FILE__)
# FileUtils.mkdir_p(ROUGE_CACHE_DIR)

module HighlightCode
  def self.highlight(str, lang, starting_line = 1)
    lang = 'ruby' if lang == 'ru'
    lang = 'objc' if lang == 'm'
    lang = 'perl' if lang == 'pl'
    lang = 'yaml' if lang == 'yml'
    str = rouge(str, lang) 
    tableize_code(str, lang, starting_line)
  end

  def self.rouge(code, lang)
    require "rouge"
    formatter = ::Rouge::Formatters::HTML.new
    formatter = ::Rouge::Formatters::HTMLLinewise.new(formatter, {class: "highlight"})
    lexer = ::Rouge::Lexer.find_fancy(lang, code) || Rouge::Lexers::PlainText
    if defined?(ROUGE_CACHE_DIR)
      path = File.join(ROUGE_CACHE_DIR, "#{lang}-#{Digest::MD5.hexdigest(code)}.html")
      if File.exist?(path)
        highlighted_code = File.read(path)
      else
        begin
          highlighted_code = formatter.format(lexer.lex(code))
        end
        File.open(path, 'w') {|f| f.print(highlighted_code) }
      end
    else
      highlighted_code = formatter.format(lexer.lex(code))
    end
    highlighted_code
  end
  def self.tableize_code (str, lang = '', starting_line = 1)
    table = '<div class="highlight"><table><tr><td class="gutter"><pre class="line-numbers">'
    code = ''
    str.lines.each_with_index do |line,index|
      # puts "code line #{index} is #{line}"
      # now get rid of those div tags
      line = line.delete_prefix("</div>")
      line = line.delete_prefix("<div class=\"highlight\">")
      line = line.tr("\r", '') if lang == 'json' # getting extra carriage returns from JSON for some reason.
      # the last line is likely blank now
      if index == str.lines.length - 1 && line == ""
        # puts "   ...skipping that one."
        next
      end
      table += "<span class='line-number'>#{index+starting_line}</span>\n"
      code  += "<span class='line'>#{line}</span>"
    end
    table += "</pre></td><td class='code'><pre><code class='#{lang}'>#{code}</code></pre></td></tr></table></div>"
  end
end
